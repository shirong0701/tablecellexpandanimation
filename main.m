//
//  main.m
//  TableCellExpandAnimation
//
//  Created by Gentle Man on 31/01/11.
//  Copyright Gentle Gentle Man 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}

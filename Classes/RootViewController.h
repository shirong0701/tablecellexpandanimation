//
//  RootViewController.h
//  TableCellExpandAnimation
//
//  Created by Gentle Man on 31/01/11.
//  Copyright Gentle Gentle Man 2011. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GTHeaderView;

@interface RootViewController : UITableViewController {
	
	NSArray* arrHeader;
	NSMutableArray* arrAddHeader;
}

- (NSArray*)indexPathsInSection:(NSInteger)section;
- (void)toggle:(BOOL*)isExpanded section:(NSInteger)section;
- (NSInteger)numberOfRowsInSection:(NSInteger)section;
-(void)toggleSectionForSection:(id)sender;


@end

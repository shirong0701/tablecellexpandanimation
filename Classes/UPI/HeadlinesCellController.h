//
//  HeadlinesCellController.h
//  UPI
//
//  Created by Gentle Man on 08/12/10.
//  Copyright 2010 Gentle Gentle Man. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HeadlinesCellController : UITableViewCell {
	
	IBOutlet UILabel* lblNews;
	IBOutlet UIImageView* imgNews;

}
@property(nonatomic,retain)UILabel* lblNews;
@property(nonatomic,retain)UIImageView* imgNews;
@end

//
//  RootViewController.m
//  TableCellExpandAnimation
//
//  Created by Gentle Man on 31/01/11.
//  Copyright Gentle Gentle Man 2011. All rights reserved.
//

#import "RootViewController.h"
#import "GTHeaderView.h"
#import "HeadlinesCellController.h"

GTHeaderView *hView;
@implementation RootViewController


-(void)generateHeaderViews{
	
	arrAddHeader = [[NSMutableArray alloc]init];
	for(int i=0;i<[arrHeader count];i++)
	{
		hView = [GTHeaderView headerViewWithTitle:[arrHeader objectAtIndex:i] section:i Expanded:@"NO"];
		[hView.button addTarget:self action:@selector(toggleSectionForSection:) forControlEvents:UIControlEventTouchUpInside];
		[arrAddHeader addObject:hView];
	}
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	self.navigationController.navigationBar.tintColor = [UIColor blackColor];
	self.navigationItem.title=@"Cell Animation";
	arrHeader = [[NSArray alloc]initWithObjects:@"TEST 1",@"TEST 2",@"TEST 3",@"TEST 4",@"TEST 5",nil];
	[self generateHeaderViews];	
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return (interfaceOrientation == UIInterfaceOrientationPortrait) | (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) | (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}
- (void)dealloc 
{
	[super dealloc];
}

#pragma mark -
#pragma mark Toggles
- (NSArray*)indexPathsInSection:(NSInteger)section 
{
	NSMutableArray *paths = [NSMutableArray array];
	NSInteger row;	
	for ( row = 0; row < [self numberOfRowsInSection:section]; row++ ) {
		[paths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
	}
	return [NSArray arrayWithArray:paths];
}
- (void)toggle:(NSString*)isExpanded:(NSInteger)section 
{
	GTHeaderView *tmp;
	tmp=[arrAddHeader objectAtIndex:section];
	if([tmp.Rowstatus isEqualToString:@"NO"])
		tmp.Rowstatus=@"YES";
	else
		tmp.Rowstatus=@"NO";
	NSArray *paths = [self indexPathsInSection:section];
	BOOL isExpand=[tmp.Rowstatus isEqualToString:@"YES"];
	if (!isExpand)
		[self.tableView deleteRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
	else
		[self.tableView insertRowsAtIndexPaths:paths withRowAnimation:UITableViewRowAnimationFade];
}
- (NSInteger)numberOfRowsInSection:(NSInteger)section 
{
	return  [arrAddHeader count];	
}
-(void)toggleSectionForSection:(id)sender
{
	GTHeaderView *tmp=[arrAddHeader objectAtIndex:[sender tag]];
	[self toggle:tmp.Rowstatus:[sender tag]];
}
#pragma mark -
#pragma mark UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return [arrAddHeader count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
	
	//Put the amount of Websites in the Account here
	int countOfSitesInAccount;
	countOfSitesInAccount = [arrAddHeader count];
	GTHeaderView *tmp=[arrAddHeader objectAtIndex:section];
	return [[tmp Rowstatus]isEqualToString:@"YES"]? countOfSitesInAccount : 0;
	
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    HeadlinesCellController *cell = (HeadlinesCellController *)[tableView dequeueReusableCellWithIdentifier:@"myidentifier"];
	if (cell == nil) 
	{
		UIViewController* mycontoller =[[UIViewController alloc] initWithNibName:@"HeadlinesCellView"bundle:nil];
		cell = (HeadlinesCellController*)mycontoller.view;
		[mycontoller release];
	}
	cell.lblNews.text = @"this is for testing ";
	
	[cell.imgNews setImage:[UIImage imageNamed:@"credit.png"]];
	return cell;	
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section 
{
	return 44;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section 
{
	return [arrAddHeader objectAtIndex:section];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning 
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}





@end

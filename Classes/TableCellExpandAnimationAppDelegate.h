//
//  TableCellExpandAnimationAppDelegate.h
//  TableCellExpandAnimation
//
//  Created by Gentle Man on 31/01/11.
//  Copyright Gentle Gentle Man 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCellExpandAnimationAppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    UINavigationController *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;

@end

